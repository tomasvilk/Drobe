import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CartContext from "components/cart/context";
import useCart from "components/cart/use-cart";
import { Navigation, Footer, Cart, ScrollToTop } from "components";
import {
  Home,
  ApieMus,
  Kontaktai,
  Pirkti,
  Produktai,
  GrazinimoTaisykles,
  PirkimoTaisykles,
  ProductPage,
  Textiles,
  Plaids,
  Wools,
  CheckOutPage,
} from "./pages";

const App = () => {
  return (
    <CartContext.Provider value={useCart([])}>
      <Router>
        <Navigation />
        <Cart />
        <main className="main">
          <ScrollToTop />
          <Switch>
            <Route exact path="/Drobe">
              <Home />
            </Route>
            <Route path="/Drobe/pirkti">
              <Pirkti />
            </Route>
            <Route path="/Drobe/produktai">
              <Produktai />
            </Route>
            <Route path="/Drobe/audiniai">
              <Textiles />
            </Route>
            <Route path="/Drobe/pledai">
              <Plaids />
            </Route>
            <Route path="/Drobe/vilna">
              <Wools />
            </Route>
            <Route path="/Drobe/apie">
              <ApieMus />
            </Route>
            <Route path="/Drobe/kontaktai">
              <Kontaktai />
            </Route>
            <Route path="/Drobe/grazinimotaisykles">
              <GrazinimoTaisykles />
            </Route>
            <Route path="/Drobe/pirkimotaisykles">
              <PirkimoTaisykles />
            </Route>
            <Route path="/Drobe/checkout">
              <CheckOutPage />
            </Route>
            <Route path="/Drobe/:id">
              <ProductPage />
            </Route>
          </Switch>
        </main>
        <Footer />
      </Router>
    </CartContext.Provider>
  );
};

export default App;
