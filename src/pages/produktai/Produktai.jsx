import { InfoSection } from "features";
import React from "react";

export const Produktai = () => {
  return (
    <div className="home-box">
      <h2 className="page-title">Produktų Informacija</h2>
      <h3 className="page-explainer">
        Visa reikalinga informacija apie &quot;Drobės&quot; tiekiamus produktus
      </h3>
      <InfoSection />
    </div>
  );
};
