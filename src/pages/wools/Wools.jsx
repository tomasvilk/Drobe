import React from "react";

export const Wools = () => {
  return (
    <div className="container">
      <div>
        <h2 className="page-title">Vilna</h2>
        <p>
          UAB „Drobė Co“, likdama ištikima Drobės tradicijoms, remiasi ilgamete
          patirtimi vilnos srityje. Mes išplėtėmė savo veiklą nuo žinomų ir
          pelniusių pagrįstą šlovę Drobės šukuotinių kostiuminių audinių iki
          vilnonių pledų iš aukštos kokybės kočiotinių verpalų gamybos.
          <br /> Vilna – mūsų veiklos pagrindas. Turėdami tiesioginius glaudžius
          kontaktus su vilnos gamintoju AvatexWool Group, mes taip pat siūlome
          vilnos, tame tarpe ir kupranugarių vilnos, tiekimo paslaugas.
        </p>
      </div>
      <div>
        <h2>Avių vilna</h2>
        <div>
          <p>
            Siūlome plautą avių vilną iš Mongolijos, Rusijos ir Kazachstano:
          </p>
          <ul>
            <li>
              <p>
                23 mic merinoso vilną, 62-65 mm, baltą ir baltą/kreminę (Rusija){" "}
              </p>
            </li>
            <li>
              <p>
                23-25 mic vilną, 62-65 mm, baltą ir baltą/šviesiai pilką
                (Rusija){" "}
              </p>
            </li>
            <li>
              <p>
                Trumpą 23-25 mic vilną, 45-55 mm, baltą/šviesiai pilką (Rusija)
              </p>
            </li>
            <li>
              <p>
                26-27 mic krosbredo vilną, 65-67 mm, baltą, baltą/šviesiai pilką
                ir rudą (Rusija)
              </p>
            </li>
            <li>
              <p>
                31-33 mic vilną, 70-75 mm, baltą ir šviesią spalvotą (Mongolija)
              </p>
            </li>
            <li>
              <p>31-33 mic vilną, 70-75 mm, rudą (Kazachstanas)</p>
            </li>
          </ul>
          <p>
            Pagal pageidavimą tiekiame prakarštą vilną, iš kurios apdirbimo metu
            pašalintas pagrindinis augalinų ir mineralinių priemaišų kiekis,
            taip pat didelė šiurkščių plaukų dalis. <br /> Ypatingas mūsų
            siūlomas produktas – avių pūkai. Jie gaunami unikalios technologijos
            dėka pašalinant iš vilnos šiurkščius plaukus. Tuo pačiu visiškai
            pašalinamos ir augalinės ir mineralinės priemaišos. Tai ypač plona
            ir visiškai švari žaliava. Galime patiekti kelių natūralių spalvų
            avių pūkus:
          </p>
          <ul>
            <li>
              <p>Baltos, pilkos ir rudos spalvų 21-22 mic, 52-55 mm</p>
            </li>
            <li>
              <p>Baltos/šviesiai pilkos spalvos 20,5-21,5 mic, 46-50 mm</p>
            </li>
          </ul>
          <p>Kilmės šalis – Mongolija.</p>
          <br />
          <p>
            Taip pat siūlome nuošukas (40-50 mic ir daugiau), kurios gaunamos
            kaip pašalinis produktas avių pūkų gamybos metu. Produktai tiekiami
            jau po muitinės formalumų sutvarkymo.
          </p>
        </div>
      </div>
      <div>
        <h2>Kupranugarių vilna</h2>
        <p>
          Siūlome įvairaus apdirbimo ir įvairaus storio kupranugarių vilną iš
          Mongolijos:
        </p>
        <ul>
          <li>
            <p>Plautą vilną</p>
          </li>
          <li>
            <p>
              Prakarštą vilną apie 31 mic, 29 mic, 26-27 mic. Tai vienarūšis
              minkštas produktas, iš kurio pašalintas pagrindinis augalinių ir
              mineralinių priemaišų kiekis
            </p>
          </li>
          <li>
            <p>
              Kupranugario pūkus, gautus pašalinant šiurkščius plaukus, o taip
              pat visas augalines ir mineralines priemaišas. Pūko storis 19-20
              mic, ilgis 42-44 mm
            </p>
          </li>
        </ul>
        <p>
          Visi kupranugario vilnos produktai siūlomi šviesiai rudos, tamsiai
          rudos ir kreminės spalvų. Pagal užsakovo pageidavimus patiekiame
          prakarštus kupranugario ir avių vilnos mišinius. <br /> Produktai
          tiekiami jau po muitinės formalumų sutvarkymo.
        </p>
      </div>
    </div>
  );
};
