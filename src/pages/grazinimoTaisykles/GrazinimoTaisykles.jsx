import React from "react";
import "./grazinimotaisykles.scss";
import rules from "../../data/rules.json";

export const GrazinimoTaisykles = () => {
  return (
    <div>
      <div className="texture-bg" />
      <div className="rule-box">
        <h2>{rules.returns.title}</h2>
        <ol>
          {rules.returns.list.map((list) => (
            <li key={list.id}>
              {list.listTitle}
              <ol>
                {list.listItems.map((item) => (
                  <li key={item.id}>{item.item}</li>
                ))}
              </ol>
            </li>
          ))}
        </ol>
      </div>
    </div>
  );
};
