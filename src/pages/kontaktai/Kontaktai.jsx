import React from "react";
import "./kontaktai.scss";
import { MapComponent } from "components";
import CallRoundedIcon from "@material-ui/icons/CallOutlined";
import WatchLaterRoundedIcon from "@material-ui/icons/WatchLaterOutlined";
import EmailRoundedIcon from "@material-ui/icons/EmailOutlined";
import RoomOutlinedIcon from "@material-ui/icons/RoomOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import { ContactForm } from "components/contactForm/ContactForm";

export const Kontaktai = () => {
  return (
    <div>
      <div className="texture-bg" />
      <div className="contact">
        <h2 className="page-title">Kontaktai</h2>
        <ul className="contact__information">
          <li className="contact__item">
            <a href="tel:+37068239712" className="contact__item-link">
              <CallRoundedIcon fontSize="large" className="contact__icon" />
              <p>+370&nbsp;682&nbsp;39712 - audiniai</p>
            </a>
          </li>
          <li className="contact__item">
            <a href="tel:+37061644014" className="contact__item-link">
              <CallRoundedIcon fontSize="large" className="contact__icon" />
              <p>+370&nbsp;616&nbsp;44014 - vilna ir pledai</p>
            </a>
          </li>
          <li className="contact__item">
            <a href="mailto:drobe@drobe.lt" className="contact__item-link">
              <EmailRoundedIcon fontSize="large" className="contact__icon" />
              <p>drobe@drobe.lt</p>
            </a>
          </li>
          <li className="contact__item">
            <WatchLaterRoundedIcon fontSize="large" className="contact__icon" />
            <p>I-V 08.30 - 16.00</p>
          </li>
          <li className="contact__item">
            <RoomOutlinedIcon fontSize="large" className="contact__icon" />
            <p>
              Draugystės&nbsp;g.&nbsp;14, LT&nbsp;-&nbsp;51259&nbsp;Kaunas,
              Lietuva
            </p>
          </li>
          <li className="contact__item">
            <a
              href="https://www.facebook.com/DrobeBlankets/"
              target="_blank"
              rel="noopener noreferrer"
              className="contact__item-link"
            >
              <FacebookIcon fontSize="large" className="contact__icon" />
              <p>@DrobeBlankets</p>
            </a>
          </li>
        </ul>
        <div className="contact__interactives">
          <MapComponent />
          <ContactForm />
        </div>
      </div>
    </div>
  );
};
