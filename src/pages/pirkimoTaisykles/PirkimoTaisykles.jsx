import React, { useLayoutEffect } from "react";
import "./pirkimotaisykles.scss";
import rules from "../../data/rules.json";

export const PirkimoTaisykles = () => {
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <div>
      <div className="texture-bg" />
      <div className="rule-box">
        <h2>{rules.sales.title}</h2>
        <h3>{rules.sales.subtitle}</h3>
        <ol>
          {rules.sales.list.map((list) => (
            <li key={list.id}>
              {list.listTitle}
              <ol>
                {list.listItems.map((item) => (
                  <li key={item.id}>{item.item}</li>
                ))}
              </ol>
            </li>
          ))}
        </ol>
      </div>
    </div>
  );
};
