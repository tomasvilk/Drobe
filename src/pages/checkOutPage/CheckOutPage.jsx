import React from "react";
import "./checkoutpage.scss";
import { CheckOutPreview, Paypal } from "components";

export const CheckOutPage = () => {
  return (
    <>
      <h2 className="page-title">Mokėjimas</h2>
      <div className="checkout">
        <CheckOutPreview />
        <Paypal />
      </div>
    </>
  );
};
