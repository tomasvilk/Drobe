import { ProductSection } from "features";
import React from "react";
import "./pirkti.scss";
import shopBG from "assets/images/hero-section-shop-image.jpg";

export const Pirkti = () => {
  return (
    <div>
      <div className="shop">
        <div className="shop__bg-container">
          <div className="shop__bg--gradient" />
          <img src={shopBG} alt="blankets" className="shop__bg" />
        </div>
        <h2 className="shop__title">Parduotuvė</h2>
      </div>
      <div className="home-box">
        <ProductSection />
      </div>
    </div>
  );
};
