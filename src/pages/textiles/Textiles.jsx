import React from "react";
import "./textiles.scss";
import audiniai from "data/audiniaiArr";
import mokykliniai from "data/mokykliniaiArr";

export const Textiles = () => {
  return (
    <div className="container">
      <div>
        <h2 className="page-title">Audiniai</h2>
        <p>
          Drobės produkciją sudaro platus madingų spalvų bei raštų audinių
          asortimentas, skirtas mokyklinių uniformų, kostiumų, švarkų, kelnių
          bei moteriškų rūbų gamybai. Įmonė taip pat gamina uniformoms, darbo
          drabužiams bei dalykinei aprangai skirtus audinius.
        </p>
        <div className="imaje-box">
          {audiniai.map((data, index) => (
            <img src={data} alt="audiniai" key={index} className="image" />
          ))}
        </div>
      </div>
      <div>
        <h2>Mokykliniai audiniai</h2>
        <p>
          Drobė siūlo platų audinių asortimentą, skirtą mokyklų bei gimnazijų
          uniformoms. Audiniai yra pagaminti iš vilnos/poliesterio bei
          vilnos/poliesterio/elastano. Tokia audinio sudėtis užtikrina jų
          kokybišką ilgalaikį dėvėjimą bei komfortabilumą, ko negali suteikti
          sintetinių pluoštų audiniai. Vilnos pluoštas puikiai išlaiko šilumą,
          tačiau neleidžia perkaisti, nedrėgsta ir leidžia odai kvėpuoti. Tokie
          audiniai yra pralaidūs orui ir saugo nuo temperatūrų kaitos.
        </p>
        <div className="imaje-box">
          {mokykliniai.map((data, index) => (
            <img src={data} alt="audiniai" key={index} className="image" />
          ))}
        </div>
      </div>
      <div>
        <h2>Uniforminiai Audiniai</h2>
        <p>
          Įmonė taip pat gamina uniformoms, darbo drabužiams bei dalykinei
          aprangai skirtus audinius. Šios kategorijos audiniai užtikrina
          ypatingai griežtus audinio kokybes parametrus ir garantuoja ilgalaikį
          audinio dėvėjimą bei kokybę. Pagal pageidavimą taikomos specialios
          apdailos. Šių audinių gamyboje naudojamos cheminės medžiagos ir kiti
          junginiai nekenksmingi vartotojo sveikatai.
        </p>
      </div>
      <div>
        <h2>Kostiuminiai Audiniai</h2>
        <p>
          &quot;Drobės&quot; produkciją sudaro plati paletė madingų spalvų,
          stilių bei raštų audinių, skirtų kelnių, kostiumų, švarkų ir moteriškų
          rūbų gamybai. Pradėti gaminti išskirtinai naujo stiliaus ir krypties
          audinius moteriškam asortimentui - šalikams, skraistėms, suknelėms,
          švarkams,ir pan. Siūlomas naujas madingas dvipusis audinys. Įmonės
          technologijos atitinka aukščiausius gamybos standartus, užtikrinama
          gaminių, nekenksmingų vartotojų sveikatai, technologija.
        </p>
      </div>
      <div>
        <h2>Kita</h2>
        <p>
          Taip pat siūlome ir kitokios įvairios paskirties audinius: <br />
          <ul>
            <li>Paltiniai</li>
            <li>Sukneliniai</li>
            <li>Pamušaliniai</li>
            <li>Proginiai</li>
            <li>Užuolaidiniai</li>
          </ul>
        </p>
      </div>
    </div>
  );
};
