import React, { useMemo } from "react";
import "./productpage.scss";
import { useParams } from "react-router-dom";
import { data } from "data/link";
import { ImageGallery, ProductInfo } from "components";

export const ProductPage = () => {
  const { id } = useParams();
  const product = useMemo(() => {
    return data.find((item) => item.link.includes(id));
  }, [id]);
  return (
    <div className="product-box">
      <ImageGallery data={product} />
      <ProductInfo product={product} id={id} />
    </div>
  );
};
