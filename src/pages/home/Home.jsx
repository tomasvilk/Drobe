import React from "react";
import "./home.scss";
import { HeroSection, InfoSection, ProductSection } from "features";
import { AboutSection } from "features/aboutSection/AboutSection";

export const Home = () => {
  return (
    <>
      <HeroSection />
      <div className="home-box">
        <ProductSection title={"Produktai"} />
        <InfoSection title={"Informacija"} />
        <AboutSection title={"Apie mus"} />
      </div>
    </>
  );
};
