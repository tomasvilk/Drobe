import React from "react";
import fotke from "assets/images/product/pledaiMerino/pledai-merino-5.jpg";
import "./plaids.scss";
import merino from "data/pledaiMerinoArr";
import vilnos from "data/pledaiVilnosArr";

export const Plaids = () => {
  return (
    <div className="container">
      <div>
        <h2 className="page-title">Pledai</h2>
        <p>
          UAB DROBĖ TEXTILE siūlo įsigyti grynavilnius pledus. Audžiame dviejų
          tipų pledus,- plonus, itin švelnius ir minkštus 100% merino vilnos
          pledus ir storesnius, šiltesnius 100% naujos, neregeneruotos vilnos ir
          Naujosios Zelandijos vilnos pledus. Ypač didžiuojamės savo unikalia
          gamybos ir taurinimo technologija, įgalinančia išgauti išskirtinius
          gaminius, kurie yra itin malonūs lytėjimui (vadinamasis &quot;kašmyro
          efektas&quot;).Patys būdami vilnos tiekėjais savo gaminiams atrenkame
          tik kokybiškiausią vilną. Mūsų gaminiai pasižymi ramiomis,
          harmoningomis spalvomis ir dizainais, yra lengvai pritaikomi bet
          kokios stilistikos interjere. Kolekcijoje visuomet turime natūralios,
          nedažytos vilnos pledų ekologiškų ir tvarių produktų gerbėjams.
        </p>
        <img src={fotke} alt="pledai" className="plaid-hero" />
      </div>
      <div>
        <h2>Merino vilnos pledai</h2>
        <p>
          Grynavilnius merino vilnos pledus gaminame naudodami unikalią
          technologiją ir taurinimo procesus, įgalinančius išgauti išskirtinius
          gaminius, kurie yra itin malonūs lytėjimui. Mūsų 100% merino vilnos
          pledai yra ypatingai ploni ir lengvi,- sveria vos po 700g., todėl itin
          tiks apsigaubti sušalus, neužims daug vietos lentynoje ar kelioniniame
          lagamine.
        </p>
        <div className="plaid-box">
          {merino[0].images.map((data, index) => (
            <img src={data} alt="merino pledas" key={index} className="plaid" />
          ))}
        </div>
      </div>
      <div>
        <h2>Avies vilnos pledai</h2>
        <p>
          Patys būdami vilnos tiekėjais savo pledams atrenkame tik pačią
          kokybiškiausią naują (neregeneruotą) avių vilną. Audžiame tik iš
          pačios švelniausios Naujosios Zelandijos ir Mongolijos avių vilnos, o
          išaustus pledus patikime ištaurinti Italijos meistrams, todėl mūsų
          pledai išsiskiria savo minkštumu ir purumu. Jie ploni, labai šilti ir
          itin lengvi.
        </p>
        <div className="plaid-box">
          {vilnos[0].images.map((data, index) => (
            <img src={data} alt="vilnos pledas" key={index} className="plaid" />
          ))}
        </div>
      </div>
    </div>
  );
};
