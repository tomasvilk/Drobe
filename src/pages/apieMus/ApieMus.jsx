import { About } from "components/about/About";
import React from "react";
import "./apiemus.scss";
import pledai from "assets/images/about-hero-image.jpg";

export const ApieMus = () => {
  return (
    <div>
      <div className="texture-bg" />
      <div className="about">
        <img src={pledai} alt="pledas kieme" className="about__bg-image" />
        <h2 className="section-title">Apie Mus</h2>
        <About />
      </div>
    </div>
  );
};
