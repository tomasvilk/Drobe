import { InfoCard } from "components";
import PropTypes from "prop-types";
import React from "react";
import "./infosection.scss";

export const InfoSection = ({ title }) => {
  return (
    <section className="info-section">
      <div>
        <h2 className="section-title">{title}</h2>
        <div className="alignment">
          <InfoCard />
        </div>
      </div>
    </section>
  );
};

InfoSection.propTypes = {
  title: PropTypes.string,
};
