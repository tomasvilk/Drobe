import React from "react";
import PropTypes from "prop-types";
import { About } from "components";

export const AboutSection = ({ title }) => {
  return (
    <section className="about-section">
      <div>
        <h2 className="section-title">{title}</h2>
        <About />
      </div>
    </section>
  );
};

AboutSection.propTypes = {
  title: PropTypes.string,
};
