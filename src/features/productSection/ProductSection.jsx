import React from "react";
import { ProductListComponent } from "components";
import PropTypes from "prop-types";
import merino from "data/merinoShopArr";
import vilnos from "data/vilnoniaiShopArr";

export const ProductSection = ({ title }) => {
  return (
    <section className="product-section">
      <div>
        <h2 className="section-title">{title}</h2>
        <div>
          <ProductListComponent data={merino} name="Merino pledai" />
          <ProductListComponent data={vilnos} name="Vilnoniai pledai" />
        </div>
      </div>
    </section>
  );
};

ProductSection.propTypes = {
  title: PropTypes.string,
};
