import React from "react";
import "./herosection.scss";
import { Link } from "react-router-dom";
import herojus from "../../assets/images/hero-section-image.jpg";

export const HeroSection = () => {
  return (
    <div className="hero">
      <div className="hero__bg-container">
        <img src={herojus} alt="priekiniam plane pledai" className="hero__bg" />
      </div>
      <div className="hero__content">
        <h2 className="hero__title">Sveiki Atvykę į jaukiausių pledų namus</h2>
        <Link to="/Drobe/pirkti" className="btn-large">
          Pirkti
        </Link>
      </div>
    </div>
  );
};
