import React from "react";
import { Link } from "react-router-dom";
import "./infocard.scss";
import infos from "../../data/infoArr";

export const InfoCard = () => {
  return (
    <>
      {infos.map((image, id) => (
        <div className="infoCard" key={id}>
          <Link to={image.link} className="infoCard__link">
            <figure className="infoCard__container">
              <img
                src={image.image}
                alt="drobe info card"
                className="infoCard__image"
              />
            </figure>
            <div className="infoCard__desc">
              <h2>{image.title}</h2>
              <p>{image.content}</p>
            </div>
          </Link>
        </div>
      ))}
    </>
  );
};
