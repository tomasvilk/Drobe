import React from "react";
import "./PaymentSuccess.scss";

export const PaymentSuccess = () => (
  <div className="Result">
    <div className="ResultTitle" role="alert">
      Apmokėjimas sėkmingas
    </div>
    <div className="ResultMessage">
      Ačiū kad pasirinkote drobę, lauksime daugiau užsakymų!
    </div>
  </div>
);
