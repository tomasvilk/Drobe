/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useContext, useState, useEffect } from "react";
import Context from "components/cart/context";
import { NavLink, useRouteMatch, useLocation } from "react-router-dom";
import "./navigation.scss";
import { ReactComponent as Logo } from "../../assets/drobe.svg";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";

export const Navigation = () => {
  const { expand, cartItems } = useContext(Context);

  const { pathname } = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  let matchPage = useRouteMatch("/Drobe");
  const [navbar, setNavbar] = useState(false);
  const [burger, setBurger] = useState(false);
  const burgerOpen = () => setBurger(!burger);

  useEffect(() => {
    const changeBg = () => {
      if (matchPage.isExact === true && window.scrollY >= 76) {
        setNavbar(true);
      } else {
        setNavbar(false);
      }
    };
    window.addEventListener("scroll", changeBg);
    changeBg();
    return () => {
      window.removeEventListener("scroll", changeBg);
    };
  }, [matchPage.isExact]);

  const location = useLocation();
  useEffect(() => {
    setBurger(false);
  }, [location]);

  return (
    <nav
      className={[
        "navbar",
        (navbar && !matchPage.isExact) || (matchPage.isExact && !navbar)
          ? "active"
          : null,
      ]
        .filter(Boolean)
        .join(" ")}
    >
      <NavLink to="/Drobe" exact className="navbar__logo-container">
        <Logo className="navbar__logo" />
      </NavLink>
      <div
        onClick={burgerOpen}
        className={burger ? "click-off" : "click-off-hidden"}
      />
      <div
        className="navbar__menu"
        role="button"
        tabIndex={0}
        onKeyDown={(e) => {
          if (e.key === "Enter") {
            burgerOpen();
          }
        }}
        onClick={burgerOpen}
      >
        <div
          className={
            burger
              ? "navbar__menu--toggle navbar__menu--line1"
              : "navbar__menu--line1"
          }
        ></div>
        <div
          className={
            burger
              ? "navbar__menu--toggle navbar__menu--line2"
              : "navbar__menu--line2"
          }
        ></div>
        <div
          className={
            burger
              ? "navbar__menu--toggle navbar__menu--line3"
              : "navbar__menu--line3"
          }
        ></div>
      </div>
      <ul className={burger ? "navbar__nav active" : "navbar__nav"}>
        <li className="navbar__item">
          <NavLink
            to="/Drobe/pirkti"
            className="navbar__link"
            activeClassName="navbar-active"
          >
            Pirkti
          </NavLink>
        </li>
        <li className="navbar__item">
          <NavLink
            to="/Drobe/produktai"
            className="navbar__link"
            activeClassName="navbar-active"
          >
            Produktai
          </NavLink>
        </li>
        <li className="navbar__item">
          <NavLink
            to="/Drobe/apie"
            className="navbar__link"
            activeClassName="navbar-active"
          >
            Apie Mus
          </NavLink>
        </li>
        <li className="navbar__item">
          <NavLink
            to="/Drobe/kontaktai"
            className="navbar__link"
            activeClassName="navbar-active"
          >
            Kontaktai
          </NavLink>
        </li>
        <li className="navbar__item">
          <ShoppingCartOutlinedIcon
            titleAccess="Krepšelis"
            role="img"
            aria-label="Krepšelis Cart"
            onClick={expand}
            tabIndex={0}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                expand();
              }
            }}
            className="navbar__cart"
          />
          <p
            className={
              cartItems.length > 0
                ? "navbar__cart--counter"
                : "navbar__cart--counter click-off-hidden "
            }
          >
            {cartItems.reduce((acc, item) => acc + item.qty, 0)}
          </p>
        </li>
      </ul>
    </nav>
  );
};
