/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import "./imagegallery.scss";
import { imageTypes } from "../../propTypes/imageType";

export const ImageGallery = ({ data }) => {
  const [selectedImg, setSelectedImage] = useState(data.images[0]);
  return (
    <div className="gallery">
      <img
        src={selectedImg}
        alt="Main selected"
        className="gallery__selected"
      />
      <div className="gallery__image">
        {data.images.map((image, index) => (
          <img
            src={image}
            key={index}
            alt="pledas"
            onClick={() => setSelectedImage(image)}
          />
        ))}
      </div>
    </div>
  );
};

ImageGallery.propTypes = {
  data: imageTypes,
};
