import React, { useState } from "react";
import PropTypes from "prop-types";
import "./productlistcomponent.scss";
import { ImageComponent } from "components";

export const ProductListComponent = ({ data, name }) => {
  const [count, setCount] = useState(4);
  const arrLength = data.length - 4;

  return (
    <>
      <h2>{name}</h2>
      <div className="list-container">
        {data.slice(0, count).map((data, index) => (
          <ImageComponent data={data} key={index} />
        ))}
      </div>
      <div className="btn-section">
        <button
          className="btn-medium dbl-btn"
          onClick={() => setCount(count + 4)}
          disabled={!data || data.length <= count ? true : false}
        >
          Daugiau
        </button>
        <button
          className="btn-medium dbl-btn"
          onClick={() => setCount(count + arrLength)}
          disabled={!data || data.length <= count ? true : false}
        >
          Rodyti Visus
        </button>
      </div>
    </>
  );
};

ProductListComponent.propTypes = {
  name: PropTypes.string,
  data: PropTypes.array,
};
