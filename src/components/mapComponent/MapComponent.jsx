import React, { useState } from "react";
import ReactMapGL, { Marker } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import "./mapcomponent.scss";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoidG9tY2p1a2FzIiwiYSI6ImNrb3c1enJ1ejAzMGQycG1wMmVuaWNpMnoifQ.RA-1vp8R0o9mW5qPe_E5dw";

export const MapComponent = () => {
  const [viewport, setViewport] = useState({
    height: window.innerWidth > 768 ? "600px" : window.innerHeight * 0.5,
    width:
      window.innerWidth > 768
        ? window.innerWidth * 0.45
        : window.innerWidth * 0.9,
    latitude: 54.906406250398256,
    longitude: 23.999202496462168,
    zoom: 15,
  });
  const [marker] = useState({
    latitude: 54.906406250398256,
    longitude: 23.999202496462168,
  });
  let size = 50;
  return (
    <ReactMapGL
      mapboxApiAccessToken={MAPBOX_TOKEN}
      mapStyle="mapbox://styles/tomcjukas/ckow7cpe00bb218ppdh39yoeo"
      {...viewport}
      onViewportChange={(nextViewport) => setViewport(nextViewport)}
      className="map"
    >
      <Marker {...marker}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 512 512"
          height={size}
          style={{
            transform: `translate(${-size / 2}px,${-size}px)`,
          }}
        >
          <path
            d="M256 0C156.698 0 76 80.7 76 180c0 33.6 9.302 66.301 27.001 94.501l140.797 230.414c2.402 3.9 6.002 6.301 10.203 6.901 5.698.899 12.001-1.5 15.3-7.2l141.2-232.516C427.299 244.501 436 212.401 436 180 436 80.7 355.302 0 256 0zm0 270c-50.398 0-90-40.8-90-90 0-49.501 40.499-90 90-90s90 40.499 90 90c0 48.9-39.001 90-90 90z"
            fill="#fd003a"
          />
          <path
            d="M256 0v90c49.501 0 90 40.499 90 90 0 48.9-39.001 90-90 90v241.991c5.119.119 10.383-2.335 13.3-7.375L410.5 272.1c16.799-27.599 25.5-59.699 25.5-92.1C436 80.7 355.302 0 256 0z"
            fill="#e50027"
          />
        </svg>
      </Marker>
    </ReactMapGL>
  );
};
