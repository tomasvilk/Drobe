import { ErrorMessage, PaymentSuccess } from "components";
import React, { useState, useRef, useEffect, useContext } from "react";
import Context from "../cart/context";
import "./paypal.scss";

export const Paypal = () => {
  const [paidFor, setPaidFor] = useState(false);
  const [error, setError] = useState(null);
  const paypalRef = useRef();
  const paypalMounted = useRef(false);
  const { removeFromCart, total, cartItems } = useContext(Context);
  const totalCart = total(cartItems);

  useEffect(() => {
    if (!paypalMounted.current) {
      paypalMounted.current = true;
      window.paypal
        .Buttons({
          createOrder: (data, actions) => {
            return actions.order.create({
              purchase_units: [
                {
                  title: "Pledai",
                  amount: {
                    currency_code: "EUR",
                    value: totalCart,
                  },
                },
              ],
            });
          },
          onApprove: async (data, actions) => {
            const order = await actions.order.capture();
            setPaidFor(true);
            console.log(order);
            cartItems.forEach((item) => {
              removeFromCart(item);
            });
          },
          onError: (err) => {
            setError(err);
            console.error(err);
          },
        })
        .render(paypalRef.current);
    }
  }, [cartItems, removeFromCart, totalCart]);

  if (paidFor) {
    return (
      <div className="paypal">
        <PaymentSuccess />
      </div>
    );
  }

  return (
    <div className="paypal">
      {error && <ErrorMessage>Netikėta klaida! - {error.message}</ErrorMessage>}
      <h2>Bendra suma: {total(cartItems).toFixed(2)}&euro;</h2>
      <div ref={paypalRef} />
    </div>
  );
};
