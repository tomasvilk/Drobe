import React, { useContext } from "react";
import "./checkoutpreview.scss";
import Context from "components/cart/context";

export const CheckOutPreview = () => {
  const { cartItems } = useContext(Context);

  return (
    <div className="checkout-preview">
      {!cartItems.length ? (
        <p className="cart__empty">Jūsų krepšelis yra tuščias!</p>
      ) : null}
      {cartItems.map((product) => (
        <div key={product.id} className="checkout-preview__itembox">
          <img src={product.mainImg} alt="pledas" />
          <div className="checkout-preview__item">
            <p className="checkout-preview__name">Produktas</p>
            <p>{product.title}</p>
          </div>
          <div className="checkout-preview__item">
            <p className="checkout-preview__name">Kiekis</p>
            <div className="checkout-preview__counter">
              <p>{product.qty}</p>
            </div>
          </div>

          <div className="checkout-preview__item">
            <p className="checkout-preview__name">Kaina</p>
            <p>{product.cost}&euro;</p>
          </div>
          <div className="checkout-preview__item">
            <p className="checkout-preview__name">Suma</p>
            <p>
              {(parseInt(product.cost) * parseInt(product.qty)).toFixed(2)}
              &euro;
            </p>
          </div>
        </div>
      ))}
    </div>
  );
};
