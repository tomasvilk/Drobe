import React from "react";
import { NavLink } from "react-router-dom";
import "./footer.scss";
import { ReactComponent as Logo } from "../../assets/drobe.svg";

export const Footer = () => {
  return (
    <footer className="footer">
      <NavLink to="/Drobe" exact className="footer__logo-container">
        <Logo className="footer__logo" />
      </NavLink>
      <nav className="footer__menu">
        <ul className="footer__nav">
          <li className="footer__item">
            <NavLink
              to="/Drobe/apie"
              className="footer__link"
              activeClassName="footer-active"
            >
              Apie Mus
            </NavLink>
          </li>
          <li className="footer__item">
            <NavLink
              to="/Drobe/kontaktai"
              className="footer__link"
              activeClassName="footer-active"
            >
              Kontaktai
            </NavLink>
          </li>
          <li className="footer__item">
            <NavLink
              to="/Drobe/pirkimotaisykles"
              className="footer__link"
              activeClassName="footer-active"
            >
              Pirkimo Taisykles
            </NavLink>
          </li>
          <li className="footer__item">
            <NavLink
              to="/Drobe/grazinimotaisykles"
              className="footer__link"
              activeClassName="footer-active"
            >
              Gražinimo Taisyklės
            </NavLink>
          </li>
        </ul>
      </nav>
      <p className="footer__copyright">&copy; Drobė, co. 2021</p>
    </footer>
  );
};
