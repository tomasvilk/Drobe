import React, { useContext } from "react";
import CartContext from "components/cart/context";
import { Link } from "react-router-dom";
import "./imagecomponent.scss";
import { imageTypes, arrTypes } from "../../propTypes/imageType";
import PropTypes from "prop-types";

export const ImageComponent = ({ data }) => {
  const { addToCart } = useContext(CartContext);
  const { title, cost, mainImg } = data;
  const id = data.link.split("/").pop();

  return (
    <figure className="image-box">
      <div className="image-name">
        {data.title} | {data.cost} &euro;
      </div>
      <Link to={data.link} className="image-link">
        <img className="image-box__image" src={data.mainImg} alt={data.title} />
      </Link>
      <button
        className="btn-cart image-cart"
        onClick={() =>
          addToCart({
            id,
            title,
            cost,
            mainImg,
          })
        }
      >
        Į krepšelį
      </button>
    </figure>
  );
};

ImageComponent.propTypes = {
  data: imageTypes,
  product: arrTypes,
  id: PropTypes.string,
  title: PropTypes.string,
  cost: PropTypes.string,
};
