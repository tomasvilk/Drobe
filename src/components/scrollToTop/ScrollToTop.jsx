import React, { useState, useEffect } from "react";
import "./scrolltotop.scss";
import KeyboardArrowUpRoundedIcon from "@material-ui/icons/KeyboardArrowUpRounded";

export const ScrollToTop = () => {
  const [showButton, setShowButton] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", checkScrollTop, { passive: true });
    return function cleanupListener() {
      window.removeEventListener("scroll", checkScrollTop);
    };
  });

  const checkScrollTop = () => {
    if (window.pageYOffset > 1000) {
      setShowButton(true);
    } else {
      setShowButton(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <KeyboardArrowUpRoundedIcon
      onClick={scrollTop}
      className="scroll-btn"
      style={{
        color: "#e0e0e0",
        height: "50px",
        width: "50px",
        display: showButton ? "block" : "none",
      }}
    />
  );
};
