import React, { useContext } from "react";
import "./productinfo.scss";
import PropTypes from "prop-types";
import { arrTypes } from "../../propTypes/imageType";
import CartContext from "components/cart/context";
import neskalbt from "assets/images/laundryWarn/neskalbt.png";
import chem from "assets/images/laundryWarn/perchloretinas.png";
import lowtemp from "assets/images/laundryWarn/lowtemp.png";
import nedziovint from "assets/images/laundryWarn/nedziovint.png";
import nebalint from "assets/images/laundryWarn/nebalint.png";

export const ProductInfo = ({ product, id }) => {
  const { addToCart } = useContext(CartContext);
  const { title, cost, mainImg } = product;

  return (
    <div className="product">
      <h2 className="product__title">{product.title}</h2>
      <p className="product__tag">{product.tag}</p>
      <p className="product__desc">{product.desc}</p>
      <p className="product__size">{product.size}</p>
      <figure className="product__laundry">
        <img src={neskalbt} alt="skalbti negalima" />
        <img src={chem} alt="sausas cheminis valymas perchloretilenu" />
        <img src={lowtemp} alt="lygint žema temperatūra" />
        <img src={nedziovint} alt="nedžiovint el. džiovyklei" />
        <img src={nebalint} alt="balinti negalima" />
        <figcaption className="freepik-cr">
          Icons made by{" "}
          <a href="https://www.freepik.com" title="Freepik">
            Freepik
          </a>{" "}
          from{" "}
          <a href="https://www.flaticon.com/" title="Flaticon">
            www.flaticon.com
          </a>
        </figcaption>
      </figure>
      <p className="product__warn">{product.warning}</p>
      <h4 className="product__cost">{product.cost}&euro;</h4>
      <button
        className="btn-medium"
        onClick={() =>
          addToCart({
            id,
            title,
            cost,
            mainImg,
          })
        }
      >
        Į krepšelį
      </button>
    </div>
  );
};

ProductInfo.propTypes = {
  product: arrTypes,
  id: PropTypes.string,
  title: PropTypes.string,
  cost: PropTypes.string,
};
