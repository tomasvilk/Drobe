/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useContext, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import "./cart.scss";
import Context from "./context";

export const Cart = () => {
  const history = useHistory();
  const { cartItems, close, changeQty, open, expand, total } = useContext(
    Context
  );

  useEffect(() => {
    const handleClose = (e) => {
      if (e.key === "Escape") {
        close();
      }
    };
    document.addEventListener("keydown", handleClose);
    return () => {
      document.removeEventListener("keydown", handleClose);
    };
  }, [close]);

  useEffect(() => {
    const listener = history.listen(() => {
      close();
    });

    return () => {
      listener();
    };
  }, [history, close]);

  return (
    <div>
      <div onClick={expand} className={open ? "overlay" : "overlay-hidden"} />
      <div className={open ? "cart active" : "cart"}>
        <h2 className="cart-title" style={{ color: "#fff" }}>
          Krepšelis
        </h2>
        <div className="cart__top">
          <p className="cart__sumcost">
            Bendra suma: {total(cartItems).toFixed(2)}&euro;
          </p>
          <Link to="/Drobe/checkout">
            <button className="btn-medium" disabled={!cartItems.length}>
              Pirmyn
            </button>
          </Link>
        </div>
        {!cartItems.length ? (
          <p className="cart__empty">Jūsų krepšelis yra tuščias!</p>
        ) : null}
        {cartItems.map((product) => (
          <div key={product.id} className="cart__itembox">
            <img src={product.mainImg} alt="pledas" />
            <div className="cart__item">
              <p className="cart__name">Produktas</p>
              <p>{product.title}</p>
            </div>
            <div className="cart__item">
              <p className="cart__name">Kiekis</p>
              <div className="cart__counter">
                <button
                  onClick={() => changeQty(product, parseInt(product.qty) - 1)}
                  className="cart__counter--btn"
                  type="button"
                  value="-"
                >
                  <span className="cart-minus">−</span>
                </button>
                <p>{product.qty}</p>
                <button
                  onClick={() => changeQty(product, parseInt(product.qty) + 1)}
                  className="cart__counter--btn"
                >
                  <span className="cart-plus">+</span>
                </button>
              </div>
            </div>

            <div className="cart__item">
              <p className="cart__name">Kaina</p>
              <p>{product.cost}&euro;</p>
            </div>
            <div className="cart__item">
              <p className="cart__name">Suma</p>
              <p>
                {(parseInt(product.cost) * parseInt(product.qty)).toFixed(2)}
                &euro;
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
