import React from "react";
import "./about.scss";

export const About = () => {
  return (
    <p className="section-text">
      &quot;Drobės&quot; vardo istorija prasideda nuo 1920 metų, kai keletas
      lietuvių kilmės verslininkų iš New York-o Kaune įkūrė &quot;Drobės&quot;
      audimo ir prekybos įmonę. Per ilgą laikotarpį nuo įkūrimo dienų,
      &quot;Drobėje&quot; įvyko daug pokyčių. &quot;Drobė&quot; yra šiuolaikiška
      tekstilės gamybinė įmonė, kurios produkciją sudaro platus madingų spalvų
      bei raštų audinių asortimentas, skirtas mokyklinių uniformų, kostiumų,
      švarkų, kelnių bei moteriškų rūbų gamybai. Įmonė turi didelę patirtį
      gaminant uniforminius ir darbo drabužiams skirtus audinius. Liekame
      ištikimi &quot;senosios&quot; &quot;Drobės&quot; tradicijoms ir remiamės
      ilgamete patirtimi vilnos srityje - išplėtėme veiklą nuo jau žinomų ir
      pelniusių pagrįstą šlovę šukuotinių kostiuminių audinių iki vilnonių pledų
      iš aukštos kokybės kočiotinių verpalų gamybos.
    </p>
  );
};
