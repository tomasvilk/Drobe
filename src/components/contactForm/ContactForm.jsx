import React from "react";
import "./contactform.scss";

export const ContactForm = () => {
  return (
    <form id="contact-form" className="form">
      <h2 className="form__title">Susisiekite su mumis!</h2>
      <div className="form-group">
        <label htmlFor="name">Vardas</label>
        <input
          type="text"
          id="name"
          className="form-control"
          autoComplete="given-name"
          placeholder="Vardas"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="lastname">Pavardė</label>
        <input
          type="text"
          id="lastname"
          className="form-control"
          placeholder="Pavardė"
          autoComplete="family-name"
        />
      </div>
      <div className="form-group">
        <label htmlFor="email">El. pašto adresas</label>
        <input
          type="email"
          id="email"
          className="form-control"
          aria-describedby="emailHelp"
          placeholder="El. paštas"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="message">Žinutė</label>
        <textarea
          className="form-control"
          placeholder="Jūsų žinutė"
          required
        ></textarea>
      </div>
      <button type="submit" className="form__btn btn-medium">
        Siųsti
      </button>
    </form>
  );
};
