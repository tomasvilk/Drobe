import argo4051 from "assets/images/shop/vilnoniai/argo-4-05-pledas-1.jpg";
import argo4052 from "assets/images/shop/vilnoniai/argo-4-05-pledas-2.jpg";
import argo4053 from "assets/images/shop/vilnoniai/argo-4-05-pledas-3.jpg";
import argo4054 from "assets/images/shop/vilnoniai/argo-4-05-pledas-4.jpg";
import derion10001 from "assets/images/shop/vilnoniai/derion-10-00-pledas-1.jpg";
import derion10002 from "assets/images/shop/vilnoniai/derion-10-00-pledas-2.jpg";
import derion10003 from "assets/images/shop/vilnoniai/derion-10-00-pledas-3.jpg";
import derion10004 from "assets/images/shop/vilnoniai/derion-10-00-pledas-4.jpg";
import eli2111 from "assets/images/shop/vilnoniai/eli-2-11-pledas-1.jpg";
import eli2112 from "assets/images/shop/vilnoniai/eli-2-11-pledas-2.jpg";
import eli2113 from "assets/images/shop/vilnoniai/eli-2-11-pledas-3.jpg";
import eli3061 from "assets/images/shop/vilnoniai/eli-3-06-pledas-1.jpg";
import eli3062 from "assets/images/shop/vilnoniai/eli-3-06-pledas-2.jpg";
import mino11011 from "assets/images/shop/vilnoniai/mino-11-01-pledas-1.jpg";
import mino11012 from "assets/images/shop/vilnoniai/mino-11-01-pledas-2.jpg";
import mino11013 from "assets/images/shop/vilnoniai/mino-11-01-pledas-3.jpg";
import mino11051 from "assets/images/shop/vilnoniai/mino-11-05-pledas-1.jpg";
import mino11052 from "assets/images/shop/vilnoniai/mino-11-05-pledas-2.jpg";
import mino11053 from "assets/images/shop/vilnoniai/mino-11-05-pledas-3.jpg";
import mino11421 from "assets/images/shop/vilnoniai/mino-11-42-pledas-1.jpg";
import mino11422 from "assets/images/shop/vilnoniai/mino-11-42-pledas-2.jpg";
import mino11423 from "assets/images/shop/vilnoniai/mino-11-42-pledas-3.jpg";
import mino11991 from "assets/images/shop/vilnoniai/mino-11-99-pledas-1.jpg";
import mino11992 from "assets/images/shop/vilnoniai/mino-11-99-pledas-2.jpg";
import mino11993 from "assets/images/shop/vilnoniai/mino-11-99-pledas-3.jpg";
import saga7001 from "assets/images/shop/vilnoniai/saga-7-00-pledas-1.jpg";
import saga7002 from "assets/images/shop/vilnoniai/saga-7-00-pledas-2.jpg";
import saga7003 from "assets/images/shop/vilnoniai/saga-7-00-pledas-3.jpg";
import saga7004 from "assets/images/shop/vilnoniai/saga-7-00-pledas-4.jpg";
import saga7101 from "assets/images/shop/vilnoniai/saga-7-10-pledas-1.jpg";
import saga7102 from "assets/images/shop/vilnoniai/saga-7-10-pledas-2.jpg";
import saga7103 from "assets/images/shop/vilnoniai/saga-7-10-pledas-3.jpg";
import saga7104 from "assets/images/shop/vilnoniai/saga-7-10-pledas-4.jpg";

export default [
  {
    mainImg: argo4051,
    title: "Argo-4-05",
    link: "/Drobe/argo405",
    desc:
      "Šilta, ryški ir džiuginanti geltona spalva sudryžuota pilkomis juostomis.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [argo4051, argo4052, argo4053, argo4054],
  },
  {
    mainImg: derion10001,
    title: "Derion-10-00",
    link: "/Drobe/derion1000",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [derion10001, derion10002, derion10003, derion10004],
  },
  {
    mainImg: eli2111,
    title: "Eli-2-11",
    link: "/Drobe/eli211",
    desc:
      "Tradicinis eglutės raštas ir monocromatiniai pilkų atspalvių langeliai baltame fone. ",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [eli2111, eli2112, eli2113],
  },
  {
    mainImg: eli3061,
    title: "Eli-3-06",
    link: "/Drobe/eli3061",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [eli3061, eli3062],
  },
  {
    mainImg: mino11011,
    title: "Mino-11-01",
    link: "/Drobe/mino1101",
    desc:
      "Klasikinis vienspalvis pledas, išaustas iš natūralios, nedažytos vilnos.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [mino11011, mino11012, mino11013],
  },
  {
    mainImg: mino11051,
    title: "Mino-11-05",
    link: "/Drobe/mino1105",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [mino11051, mino11052, mino11053],
  },
  {
    mainImg: mino11421,
    title: "Mino-11-42",
    link: "/Drobe/mino1142",
    desc:
      "Klasikinio rašto pledas, išaustas iš natūralios, nedažytos rudos ir melsvos vilnos. ",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [mino11421, mino11422, mino11423],
  },
  {
    mainImg: mino11991,
    title: "Mino-11-99",
    link: "/Drobe/mino1199",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [mino11991, mino11992, mino11993],
  },
  {
    mainImg: saga7001,
    title: "Saga-7-00",
    link: "/Drobe/saga700",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [saga7001, saga7002, saga7003, saga7004],
  },
  {
    mainImg: saga7101,
    title: "Saga-7-10",
    link: "/Drobe/saga710",
    desc:
      "Santūrus grafinis dizainas lengvai derės kiekviename interjere, mėlyna spalva suteiks modernumo klasikiniams raštams.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100%  naujos avies vilnos, itin jaukus, švelnus ir šiltas pledas!",
    cost: "49.00 ",
    images: [saga7101, saga7102, saga7103, saga7104],
  },
];
