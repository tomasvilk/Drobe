import arina3001 from "assets/images/shop/merino/arina-3-00-pledas-1.jpg";
import arina3002 from "assets/images/shop/merino/arina-3-00-pledas-2.jpg";
import arina3003 from "assets/images/shop/merino/arina-3-00-pledas-3.jpg";
import arina3004 from "assets/images/shop/merino/arina-3-00-pledas-4.jpg";
import arina3005 from "assets/images/shop/merino/arina-3-00-pledas-5.jpg";
import arina3006 from "assets/images/shop/merino/arina-3-00-pledas-6.jpg";
import arina3007 from "assets/images/shop/merino/arina-3-00-pledas-7.jpg";
import brian4011 from "assets/images/shop/merino/brian-4-01-pledas-1.jpg";
import brian4012 from "assets/images/shop/merino/brian-4-01-pledas-2.jpg";
import brian4013 from "assets/images/shop/merino/brian-4-01-pledas-3.jpg";
import classic7021 from "assets/images/shop/merino/classic-7-02-pledas-1.jpg";
import classic7022 from "assets/images/shop/merino/classic-7-02-pledas-2.jpg";
import classic7023 from "assets/images/shop/merino/classic-7-02-pledas-3.jpg";
import classic7024 from "assets/images/shop/merino/classic-7-02-pledas-4.jpg";
import classic7031 from "assets/images/shop/merino/classic-7-03-pledas-1.jpg";
import classic7032 from "assets/images/shop/merino/classic-7-03-pledas-2.jpg";
import classic7033 from "assets/images/shop/merino/classic-7-03-pledas-3.jpg";
import classic7034 from "assets/images/shop/merino/classic-7-03-pledas-4.jpg";
import classic7041 from "assets/images/shop/merino/classic-7-04-pledas-1.jpg";
import classic7042 from "assets/images/shop/merino/classic-7-04-pledas-2.jpg";
import classic7043 from "assets/images/shop/merino/classic-7-04-pledas-3.jpg";
import classic7101 from "assets/images/shop/merino/classic-7-10-pledas-1.jpg";
import classic7102 from "assets/images/shop/merino/classic-7-10-pledas-2.jpg";
import classic7103 from "assets/images/shop/merino/classic-7-10-pledas-3.jpg";
import classic7104 from "assets/images/shop/merino/classic-7-10-pledas-4.jpg";
import classic7105 from "assets/images/shop/merino/classic-7-10-pledas-5.jpg";
import classic7111 from "assets/images/shop/merino/classic-7-11-pledas-1.jpg";
import classic7112 from "assets/images/shop/merino/classic-7-11-pledas-2.jpg";
import classic7113 from "assets/images/shop/merino/classic-7-11-pledas-3.jpg";
import classic7114 from "assets/images/shop/merino/classic-7-11-pledas-4.jpg";
import classic7115 from "assets/images/shop/merino/classic-7-11-pledas-5.jpg";
import classic7121 from "assets/images/shop/merino/classic-7-12-pledas-1.jpg";
import classic7122 from "assets/images/shop/merino/classic-7-12-pledas-2.jpg";
import classic7123 from "assets/images/shop/merino/classic-7-12-pledas-3.jpg";
import classic7124 from "assets/images/shop/merino/classic-7-12-pledas-4.jpg";
import classic7125 from "assets/images/shop/merino/classic-7-12-pledas-5.jpg";
import classic7161 from "assets/images/shop/merino/classic-7-16-pledas-1.jpg";
import classic7162 from "assets/images/shop/merino/classic-7-16-pledas-2.jpg";
import classic7163 from "assets/images/shop/merino/classic-7-16-pledas-3.jpg";
import classic7181 from "assets/images/shop/merino/classic-7-18-pledas-1.jpg";
import classic7182 from "assets/images/shop/merino/classic-7-18-pledas-2.jpg";
import classic7183 from "assets/images/shop/merino/classic-7-18-pledas-3.jpg";
import classic7184 from "assets/images/shop/merino/classic-7-18-pledas-4.jpg";
import classic7191 from "assets/images/shop/merino/classic-7-19-pledas-1.jpg";
import classic7192 from "assets/images/shop/merino/classic-7-19-pledas-2.jpg";
import classic7193 from "assets/images/shop/merino/classic-7-19-pledas-3.jpg";
import classic7194 from "assets/images/shop/merino/classic-7-19-pledas-4.jpg";
import classic7195 from "assets/images/shop/merino/classic-7-19-pledas-5.jpg";
import claudia17011 from "assets/images/shop/merino/claudia-17-01-pledas-1.jpg";
import claudia17012 from "assets/images/shop/merino/claudia-17-01-pledas-2.jpg";
import claudia17013 from "assets/images/shop/merino/claudia-17-01-pledas-3.jpg";
import claudia17014 from "assets/images/shop/merino/claudia-17-01-pledas-4.jpg";
import claudia17071 from "assets/images/shop/merino/claudia-17-07-pledas-1.jpg";
import claudia17072 from "assets/images/shop/merino/claudia-17-07-pledas-2.jpg";
import etno1001 from "assets/images/shop/merino/etno-1-00-pledas-1.jpg";
import etno1002 from "assets/images/shop/merino/etno-1-00-pledas-2.jpg";
import etno1111 from "assets/images/shop/merino/etno-1-11-pledas-1.jpg";
import etno1112 from "assets/images/shop/merino/etno-1-11-pledas-2.jpg";
import etno1113 from "assets/images/shop/merino/etno-1-11-pledas-3.jpg";
import etno5111 from "assets/images/shop/merino/etno-5-11-pledas-1.jpg";
import etno5112 from "assets/images/shop/merino/etno-5-11-pledas-2.jpg";
import etno5113 from "assets/images/shop/merino/etno-5-11-pledas-3.jpg";

export default [
  {
    mainImg: arina3001,
    title: "Arina-3-00",
    link: "/Drobe/arina300",
    desc:
      "Baltos ir rusvos spalvos unikalaus dizaino  pledas. Dvipusis,- viena pusė  balti kvadratėliai rusvame fone, kita-  rusvi  baltame!  Itin plonas ir lengvas,",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [
      arina3001,
      arina3002,
      arina3003,
      arina3004,
      arina3005,
      arina3006,
      arina3007,
    ],
  },
  {
    mainImg: brian4011,
    title: "Brian-4-01",
    link: "/Drobe/brian401",
    desc:
      "Baltos ir pilkos spalvos  unikalaus dizaino  pledas. Dvipusis, -  viena pusė kitos negatyvas!  Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [brian4011, brian4012, brian4013],
  },
  {
    mainImg: classic7021,
    title: "Classic-7-02",
    link: "/Drobe/classic702",
    desc:
      "Klasikinis smėlio spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7021, classic7022, classic7023, classic7024],
  },
  {
    mainImg: classic7031,
    title: "Classic-7-03",
    link: "/Drobe/classic703",
    desc:
      "Klasikinis rudos spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7031, classic7032, classic7033, classic7034],
  },
  {
    mainImg: classic7041,
    title: "Classic-7-04",
    link: "/Drobe/classic704",
    desc:
      "Klasikinis jūros bangos spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7041, classic7042, classic7043],
  },
  {
    mainImg: classic7101,
    title: "Classic7101",
    link: "/Drobe/classic710",
    desc:
      "Klasikinis šviesiai pilkos spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7101, classic7102, classic7103, classic7104, classic7105],
  },
  {
    mainImg: classic7111,
    title: "Classic-7-11",
    link: "/Drobe/classic711",
    desc:
      "Klasikinis tamsiai pilkos spalvos eglutės audimo pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7111, classic7112, classic7113, classic7114, classic7115],
  },
  {
    mainImg: classic7121,
    title: "Classic-7-12",
    link: "/Drobe/classic712",
    desc:
      "Klasikinis rudos spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7121, classic7122, classic7123, classic7124, classic7125],
  },
  {
    mainImg: classic7161,
    title: "Classic-7-16",
    link: "/Drobe/classic716",
    desc:
      "Klasikinis žalsvos spalvos eglutės audimo  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7161, classic7162, classic7163],
  },
  {
    mainImg: classic7181,
    title: "Classic-7-18",
    link: "/Drobe/classic718",
    desc:
      "Itil švelnios rausvai smėlinės spalvos eglutės dizaino  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7181, classic7182, classic7183, classic7184],
  },
  {
    mainImg: classic7191,
    title: "Classic-7-19",
    link: "/Drobe/classic719",
    desc:
      "Klasikinis žalsvos spalvos eglutės audimo  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [classic7191, classic7192, classic7193, classic7194, classic7195],
  },
  {
    mainImg: claudia17011,
    title: "Claudia-17-01",
    link: "/Drobe/claudia1701",
    desc:
      "Klasikinis tamsiai pilkos spalvos eglutės audimo pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [claudia17011, claudia17012, claudia17013, claudia17014],
  },
  {
    mainImg: claudia17071,
    title: "Claudia-17-07",
    link: "/Drobe/claudia1707",
    desc:
      "Itil švelnios rausvai smėlinės spalvos  pledas. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [claudia17071, claudia17072],
  },
  {
    mainImg: etno1001,
    title: "Etno-1-00",
    link: "/Drobe/etno100",
    desc:
      "Mūsų unikalaus pynimo dvipusis pledas,- viena pusė  jaukaus rusvo atspalvio, kita,- pieno baltumo. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [etno1001, etno1002],
  },
  {
    mainImg: etno1111,
    title: "Etno-1-11",
    link: "/Drobe/etno111",
    desc:
      "Mūsų unikalaus pynimo dvipusis pledas,- viena pusė pilkos spalvos, kita,- pieno baltumo. Itin plonas ir lengvas.",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [etno1111, etno1112, etno1113],
  },
  {
    mainImg: etno5111,
    title: "Etno-5-11",
    link: "/Drobe/etno511",
    desc: "",
    size: "Dydis: 140x200 cm, įskaitant ir kutus.",
    warning:
      "Nepamirškite, jog spalvos monitoriuje gali skirtis nuo spalvų tikrovėje.",
    tag: "100% merino vilnos, 100% švelnumo, jaukumo ir šilumos!",
    cost: "69.00",
    images: [etno5111, etno5112, etno5113],
  },
];
