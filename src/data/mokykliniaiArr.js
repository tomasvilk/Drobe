import mokykliniai1 from "assets/images/product/mokykliniai/audiai-mokykliniai-1.jpg";
import mokykliniai10 from "assets/images/product/mokykliniai/audiai-mokykliniai-10.jpg";
import mokykliniai11 from "assets/images/product/mokykliniai/audiai-mokykliniai-11.jpg";
import mokykliniai12 from "assets/images/product/mokykliniai/audiai-mokykliniai-12.jpg";
import mokykliniai13 from "assets/images/product/mokykliniai/audiai-mokykliniai-13.jpg";
import mokykliniai14 from "assets/images/product/mokykliniai/audiai-mokykliniai-14.jpg";
import mokykliniai15 from "assets/images/product/mokykliniai/audiai-mokykliniai-15.jpg";
import mokykliniai16 from "assets/images/product/mokykliniai/audiai-mokykliniai-16.jpg";
import mokykliniai17 from "assets/images/product/mokykliniai/audiai-mokykliniai-17.jpg";
import mokykliniai2 from "assets/images/product/mokykliniai/audiai-mokykliniai-2.jpg";
import mokykliniai3 from "assets/images/product/mokykliniai/audiai-mokykliniai-3.jpg";
import mokykliniai4 from "assets/images/product/mokykliniai/audiai-mokykliniai-4.jpg";
import mokykliniai5 from "assets/images/product/mokykliniai/audiai-mokykliniai-5.jpg";
import mokykliniai6 from "assets/images/product/mokykliniai/audiai-mokykliniai-6.jpg";
import mokykliniai7 from "assets/images/product/mokykliniai/audiai-mokykliniai-7.jpg";
import mokykliniai8 from "assets/images/product/mokykliniai/audiai-mokykliniai-8.jpg";
import mokykliniai9 from "assets/images/product/mokykliniai/audiai-mokykliniai-9.jpg";

export default [
  mokykliniai1,
  mokykliniai2,
  mokykliniai3,
  mokykliniai4,
  mokykliniai5,
  mokykliniai6,
  mokykliniai7,
  mokykliniai8,
  mokykliniai9,
  mokykliniai10,
  mokykliniai11,
  mokykliniai12,
  mokykliniai13,
  mokykliniai14,
  mokykliniai15,
  mokykliniai16,
  mokykliniai17,
];
