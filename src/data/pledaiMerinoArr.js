import merino1 from "assets/images/product/pledaiMerino/pledai-merino-1.jpg";
import merino2 from "assets/images/product/pledaiMerino/pledai-merino-2.jpg";
import merino3 from "assets/images/product/pledaiMerino/pledai-merino-3.jpg";
import merino4 from "assets/images/product/pledaiMerino/pledai-merino-4.jpg";
import merino5 from "assets/images/product/pledaiMerino/pledai-merino-5.jpg";
import merino6 from "assets/images/product/pledaiMerino/pledai-merino-6.jpg";

export default [
  {
    title: "Merino Vilnos Pledai",
    desc:
      "Grynavilnius merino vilnos pledus gaminame naudodami unikalią technologiją ir taurinimo procesus, įgalinančius išgauti išskirtinius gaminius, kurie yra itin malonūs lytėjimui. Mūsų  100% merino vilnos pledai yra ypatingai ploni ir lengvi,- sveria vos po 700g., todėl itin tiks apsigaubti sušalus, neužims daug vietos  lentynoje ar kelioniniame lagamine.",
    images: [merino1, merino2, merino3, merino4, merino5, merino6],
  },
];
