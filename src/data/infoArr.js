import wools from "../assets/images/product-card-image.jpg";
import fabrics from "../assets/images/product/audiniai/audiniai-main-8.jpg";
import blankets from "../assets/images/product/pledaiMerino/pledai-merino-5.jpg";

export default [
  {
    image: fabrics,
    title: "Audiniai",
    link: "/Drobe/audiniai",
    content:
      "Platus madingų spalvų bei raštų audinių asortimentas skirtas mokyklinių uniformų, kostiumų, švarkų, kelnių bei moteriškų rūbų gamybai",
  },
  {
    image: blankets,
    title: "Pledai",
    link: "/Drobe/pledai",
    content:
      "Audžiame trijų tipų pledus - plonus itin švelnius ir minkštus merino vilnos pledus, storesnius šiltesnius naujos, neregeneruotos vilnos ir Naujosios Zelandijos vilnos pledus.",
  },
  {
    image: wools,
    title: "Vilna",
    link: "/Drobe/vilna",
    content:
      "Išlikdami ištikimi Drobės tradicijoms, mes remiamės ilgamete patirtimi vilnos srityje.",
  },
];
