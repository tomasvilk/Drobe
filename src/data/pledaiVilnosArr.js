import vilnos1 from "assets/images/product/pledaiVilnos/pledai-vilna-1.jpg";
import vilnos2 from "assets/images/product/pledaiVilnos/pledai-vilna-2.jpg";
import vilnos3 from "assets/images/product/pledaiVilnos/pledai-vilna-3.jpg";
import vilnos4 from "assets/images/product/pledaiVilnos/pledai-vilna-4.jpg";
import vilnos5 from "assets/images/product/pledaiVilnos/pledai-vilna-5.jpg";
import vilnos6 from "assets/images/product/pledaiVilnos/pledai-vilna-6.jpg";

export default [
  {
    title: "Avies Vilnos Pledai",
    desc:
      "Patys būdami vilnos tiekėjais savo pledams atrenkame tik pačią kokybiškiausią naują (neregeneruotą) avių vilną. Audžiame tik iš pačios švelniausios Naujosios Zelandijos ir Mongolijos avių vilnos, o išaustus pledus patikime ištaurinti Italijos meistrams, todėl mūsų pledai išsiskiria savo minkštumu ir purumu. Jie ploni, labai šilti ir itin lengvi.",
    images: [vilnos1, vilnos2, vilnos3, vilnos4, vilnos5, vilnos6],
  },
];
