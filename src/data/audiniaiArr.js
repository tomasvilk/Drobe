import audiniai1 from "assets/images/product/audiniai/audiniai-main-1.jpg";
import audiniai2 from "assets/images/product/audiniai/audiniai-main-2.jpg";
import audiniai3 from "assets/images/product/audiniai/audiniai-main-3.jpg";
import audiniai4 from "assets/images/product/audiniai/audiniai-main-4.jpg";
import audiniai5 from "assets/images/product/audiniai/audiniai-main-5.jpg";
import audiniai6 from "assets/images/product/audiniai/audiniai-main-6.jpg";
import audiniai7 from "assets/images/product/audiniai/audiniai-main-7.jpg";
import audiniai8 from "assets/images/product/audiniai/audiniai-main-8.jpg";

export default [
  audiniai1,
  audiniai2,
  audiniai3,
  audiniai4,
  audiniai5,
  audiniai6,
  audiniai7,
  audiniai8,
];
