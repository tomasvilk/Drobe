import PropTypes from "prop-types";

export const imageTypes = PropTypes.shape({
  mainImg: PropTypes.string,
  title: PropTypes.string,
});

export const arrTypes = PropTypes.shape({
  mainImg: PropTypes.string,
  title: PropTypes.string,
  link: PropTypes.string,
  desc: PropTypes.string,
  size: PropTypes.string,
  warning: PropTypes.string,
  tag: PropTypes.string,
  cost: PropTypes.string,
  images: PropTypes.arrayOf[PropTypes.string],
});
